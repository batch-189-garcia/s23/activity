// Creating an object constructor
function Pokemon (name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){


		console.log(this.name + " tackled " + target.name);

		// Added  below
		console.log("target.health before tackle is " + target.health);
		console.log("this.attack power is " + this.attack);
		
		
		// Added below line
		target.health = target.health - this.attack; 
		
		console.log(target.name + "'s health is now reduced to " + (target.health));

		// Added  below
		console.log("target.health is " + target.health);
		
		if (target.health <= 5) {target.faint()}


	}, 
	
	this.faint = function() {
		console.log(this.name + " fainted. ")
	}


}


let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon ("Squirtle", 8);


console.log(pikachu);
console.log(squirtle);

// console.log("tackle1");

// pikachu.tackle(squirtle);
// console.log(squirtle);

// console.log("tackle2");
// pikachu.tackle(squirtle);
// console.log(squirtle);



console.log("tackle1");

squirtle.tackle(pikachu);
console.log(pikachu);

console.log("tackle2");
squirtle.tackle(pikachu);
console.log(pikachu);

console.log("tackle3");
squirtle.tackle(pikachu);
console.log(pikachu);

console.log("tackle4");
squirtle.tackle(pikachu);
console.log(pikachu);

console.log("tackle5");
squirtle.tackle(pikachu);
console.log(pikachu);

console.log("tackle6");
squirtle.tackle(pikachu);
console.log(pikachu);

// squirtle.faint();


/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function


*/